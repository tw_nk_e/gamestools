## 1.8.1 (01 janvier 2022)
  - [6e72ae3 : Maxime DEVOULX] Update home page.

## 1.7.1 (01 janvier 2022)
  - [2eb4a2a : Maxime DEVOULX] Add license.

## 1.6.1 (01 janvier 2022)
  - [d793dd1 : Maxime DEVOULX] Update home page.

## 1.5.1 (01 janvier 2022)
  - [9b8fd15 : Maxime DEVOULX] Fix Destiny 2 Configuration.

## 1.5.0 (01 janvier 2022)
  - [8bb221e : Maxime DEVOULX] Update Destiny 2 Configuration.
  - [cf51d4f : Maxime DEVOULX] Add Steam Launch Options.

## 1.4.0 (21 décembre 2021)
  - [e57cbc5 : Maxime DEVOULX] Update 'README.md' for Destiny 2.

## 1.3.0 (21 décembre 2021)
  - [3b85fc2 : Maxime DEVOULX] Fix extract all commit.
  - [6417b58 : Maxime DEVOULX] Rename 'Destiny 2' to 'destiny_2'. Add Destiny 2 PvP maps. Add Git LFS '*.jpg'.

## 1.2.0 (21 décembre 2021)
  - [798d194 : Maxime DEVOULX] Update 'destiny_2_configuration.md'.
  - [7b8bb12 : Maxime DEVOULX] Destiny 2 source.

## 1.1.0 (20 décembre 2021)
  - [dbea5d5 : Maxime DEVOULX] Init Git LFS.

## 1.0.0 (20 décembre 2021)
  - [1050254 : Maxime DEVOULX] Create 'destiny_2_configuration.md'.

## 0.0.0 (20 décembre 2021)
  - [8af8e83 : Maxime DEVOULX] Initisalization of the project.

