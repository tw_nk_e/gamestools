<dl>
	<h1 align="center">
		<a href="https://gitlab.com/tw_nk_e/gamestools">
			<img alt="Games Tools" src="https://gitlab.com/tw_nk_e/gamestools/-/raw/main/src/games_tools.png" width="400">
		</a>
		<br>Games Tools <br>
	</h1>
</dl>

<dl>
	<p align="center">
		<a href="https://gitlab.com/tw_nk_e/gamestools/-/tags">
			<img alt="Tags" src="https://img.shields.io/gitlab/v/tag/32272589?label=version&style=flat-square">
		</a>
		<a href="https://gitlab.com/tw_nk_e/gamestools/-/raw/main/LICENSE">
			<img alt="License" src="https://img.shields.io/badge/license-TBW%20-yellow?style=flat-square">
		<a href="https://steamcommunity.com/id/tw_nk_e/">
			<img alt="Steam" src="https://img.shields.io/badge/steam-tw__nk__e-171A20?style=flat-square">
		</a>
		<a href="https://discord.com/">
			<img alt="Discord" src="https://img.shields.io/badge/discord-tw__nk__e%233576-5969E9?style=flat-square">
		</a>
	</p>
</dl>

<div align="center">
	<sub>Built with ❤︎ by tw_nk_e.</sub>
</div>
<br>

GamesTools est un repo regoupant quelques astuces. Il vous permetra (peu être 🤓) d'optimiser au mieux vos jeux ou votre PC afin d'avoir la meilleur expérience possible. Il regoupe aussi l'ensemble des "sources" que j'utilise réguliérement.  
J'espère vous avoir aidez, enjoy 🙃 !  

## Destiny 2

- [Destiny 2 Configuration](./destiny_2/destiny_2_configuration.md)  
- [Destiny 2 PvP Maps](./destiny_2/pvp_maps)