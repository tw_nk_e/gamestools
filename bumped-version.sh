#!/bin/bash
#/*
# * ----------------------------------------------------------------------------
# * "THE BEER-WARE LICENSE" (Revision 42):
# * <phk@FreeBSD.ORG> wrote this file.  As long as you retain this notice you
# * can do whatever you want with this stuff. If we meet some day, and you think
# * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
# * ----------------------------------------------------------------------------
# */

# ============================
# ********** SOURCE **********
# ============================

# https://gist.github.com/Nomane/df017387aaa2d24cbacb5da3a55256cf
# [Get all git commits since last tag](https://stackoverflow.com/questions/12082981/get-all-git-commits-since-last-tag)

# ==============================
# ********** VARIABLE **********
# ==============================

#BLACK="\033[0;30m"
#RED="\033[0;31m"
#GREEN="\033[0;32m"
#BROWN="\033[0;33m"
#BLUE="\033[0;34m"
#PURPLE="\033[0;35m"
#CYAN="\033[0;36m"
#GRAY="\033[0;37m"
#LIGHTBLACK="\033[1;30m"
LIGHTRED="\033[1;31m"
LIGHTGREEN="\033[1;32m"
LIGHTBROWN="\033[1;33m"
#LIGHTBLUE="\033[1;34m"
#LIGHTPURPLE="\033[1;35m"
LIGHTCYAN="\033[1;36m"
LIGHTGRAY="\033[1;37m"
#RESETCOLOR="\033[0m"

NOTICE_FLAG="${LIGHTCYAN}❯"
QUESTION_FLAG="${LIGHTBROWN}?"
WARNING_FLAG="${LIGHTRED}!"
BASH_FLAG="${LIGHTGREEN}$"
UNICORN="${LIGHTRED}\xf0\x9f\xa6\x84\x0a"

DATE="$(date +'%d %B %Y')"
LATEST_HASH=`git log --pretty=format:'%h' -n 1`
CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
VERSION_FORMAT='^[0-9]+\.[0-9]+\.[0-9]+$'

ADJUSTMENTS_MSG="${NOTICE_FLAG} Now you can make adjustments to ${LIGHTGREEN}CHANGELOG.md${LIGHTCYAN}. Press enter to continue." 
#PUSHING_MSG="${NOTICE_FLAG} Pushing new version to the ${WHITE}origin${PURPLE}..."
#RELEASE_NOTE=""

# ============================
# ********** SCRIPT **********
# ============================

if [ -f VERSION ]; then # if "VERSION" file exist
    CURRENT_BRANCH=(`git rev-parse --abbrev-ref HEAD`) # read Git branch
    NEW_FORMAT_BRANCH=(`echo ${CURRENT_BRANCH} | tr '-' ' '`) # convert 'azerty-...' to 'azerty'
    CURRENT_VERSION=(`cat VERSION`) # read VERSION
    NEW_FORMAT_VERSION=(`echo ${CURRENT_VERSION} | tr '.' ' '`) # convert '1.1.1' to '1 1 1'
    V_MAJOR=${NEW_FORMAT_VERSION[0]} # -----------------------------------------------| | |
    V_MINOR=${NEW_FORMAT_VERSION[1]} # -------------------------------------------------| |
    V_PATCH=${NEW_FORMAT_VERSION[2]} # ---------------------------------------------------|
    # displays information for user
    echo -e "${NOTICE_FLAG} Latest commit hash: ${LIGHTGREEN}$LATEST_HASH"
    echo -e "${NOTICE_FLAG} Current version: ${LIGHTGREEN}$CURRENT_VERSION"
    echo -e "${NOTICE_FLAG} Current branch: ${LIGHTGREEN}$CURRENT_BRANCH"
    # auto new version
    if [[ "$NEW_FORMAT_BRANCH" == "release" ]]; then V_MAJOR=$((V_MAJOR + 1)); fi
    if [[ "$NEW_FORMAT_BRANCH" == "feature" ]] || [[ "$NEW_FORMAT_BRANCH" == "update" ]]; then V_MINOR=$((V_MINOR + 1)); fi
    if [[ "$NEW_FORMAT_BRANCH" == "hotfix" ]] || [[ "$NEW_FORMAT_BRANCH" == "bugfix" ]]; then V_PATCH=$((V_PATCH + 1)); fi
    SUGGESTED_VERSION="${V_MAJOR}.${V_MINOR}.${V_PATCH}"
    # the new version number
    echo -ne "${QUESTION_FLAG} Enter a version number [${LIGHTGREEN}$SUGGESTED_VERSION${LIGHTBROWN}]: ${LIGHTGRAY}"
    read INPUT_STRING
    if [[ "$INPUT_STRING" == "" ]]; then INPUT_STRING=${SUGGESTED_VERSION}; fi
    # check "INPUT_STRING" 
    until [[ "${INPUT_STRING}" =~ ${VERSION_FORMAT} ]]; do
          echo -e "${WARNING_FLAG} Invalid version number!"
          echo -ne "${QUESTION_FLAG} Please, enter a valid format [${LIGHTGREEN}$SUGGESTED_VERSION${LIGHTBROWN}]: ${LIGHTGRAY}"
          read INPUT_STRING
    done  
    echo -e "${NOTICE_FLAG} Will set new version to be ${LIGHTGREEN}$INPUT_STRING"
    echo $INPUT_STRING > VERSION
    echo "## $INPUT_STRING ($DATE)" > tmpfile
    # git log --pretty=format:"  - [%h : %an] %s" "v$CURRENT_VERSION"...HEAD >> tmpfile
    git log --pretty=format:"  - [%h : %an] %s" $(git describe --tags --abbrev=0)..HEAD >> tmpfile
    echo "" >> tmpfile
    echo "" >> tmpfile
    cat CHANGELOG.md >> tmpfile
    mv tmpfile CHANGELOG.md
    echo -e "$ADJUSTMENTS_MSG"
    read
    vim CHANGELOG.md
else
    echo -e "${WARNING_FLAG} Could not find a VERSION file."
    echo -ne "${QUESTION_FLAG} Do you want to create a version file and start from scratch? [y]: ${LIGHTGRAY}"
    read RESPONSE
    if [[ "$RESPONSE" == "" ]]; then RESPONSE="y"; fi
    if [[ "$RESPONSE" == "Y" ]]; then RESPONSE="y"; fi
    if [[ "$RESPONSE" == "Yes" ]]; then RESPONSE="y"; fi
    if [[ "$RESPONSE" == "yes" ]]; then RESPONSE="y"; fi
    if [[ "$RESPONSE" == "YES" ]]; then RESPONSE="y"; fi
    if [[ "$RESPONSE" == "y" ]]; then
        echo -ne "${QUESTION_FLAG} Enter a version number [x.x.x]: ${LIGHTGRAY}"
        read INPUT_STRING
        echo -e "${NOTICE_FLAG} The new version is ${LIGHTGREEN}$INPUT_STRING"
        echo $INPUT_STRING > VERSION
        echo "## $INPUT_STRING ($DATE)" > CHANGELOG.md
        git log --pretty=format:"  - [%h : %an] %s" >> CHANGELOG.md
        echo "" >> CHANGELOG.md
        echo "" >> CHANGELOG.md
        echo -e "$ADJUSTMENTS_MSG"
        read
        vim CHANGELOG.md
    fi
fi
echo -e "${NOTICE_FLAG} Finished."
echo -e "${NOTICE_FLAG} Next step:"
echo -e "${WARNING_FLAG} (GitHub-Flow)"
echo -e "   ${BASH_FLAG} git commit -a -m 'bumped version number to $INPUT_STRING
     > on branch ./$CURRENT_BRANCH from ./master'"
echo -e "   ${BASH_FLAG} git checkout master"
echo -e "   ${BASH_FLAG} git merge --no-ff $CURRENT_BRANCH"
echo -e "   ${BASH_FLAG} git tag -a $INPUT_STRING"
echo -e "   ${BASH_FLAG} git branch -d $CURRENT_BRANCH"
echo -e "   ${BASH_FLAG} git push origin master --tags"
echo -e "${UNICORN} Enjoy! "