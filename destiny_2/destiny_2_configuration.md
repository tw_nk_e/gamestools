# Destiny 2 Configuration

[[_TOC_]]

## Destiny 2 Graphics Settings
### Field of View

Si vous voulez voir plus de monde à la fois, en particulier sur les périphéries de votre écran, vous devez augmenter le champ de vision (FoV). L'inconvénient est que les objets au milieu de l'écran apparaîtront plus petits lorsque le FoV est augmenté, ce qui les rend plus difficiles à voir ou à viser. En général, être capable de voir plus autour de vous est la meilleure façon de jouer, car cela réduit le nombre de rotations constantes nécessaires pour voir les ennemis qui attaquent sur les côtés. [3]  

<img alt="D2 Field of View" src="src/destiny-2-field-of-view-performance.png" title="Destiny 2 Field of View Performance" width="640" height="421">

<ins>Interactive Comparisons :</ins>
- [105 Degrees vs. 95 Degrees](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-field-of-view-interactive-comparison-001-105-vs-95.html)  
- [105 Degrees vs. 85 Degrees](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-field-of-view-interactive-comparison-001-105-vs-85.html)  
- [105 Degrees vs. 75 Degrees](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-field-of-view-interactive-comparison-001-105-vs-75.html)  
- [105 Degrees vs. 65 Degrees](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-field-of-view-interactive-comparison-001-105-vs-65.html)  
- [105 Degrees vs. 55 Degrees](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-field-of-view-interactive-comparison-001-105-vs-55.html)  

<ins>Avantages et inconvénients du FoV</ins>

Selon le mythe du jeu sur PC, une valeur FoV élevée est toujours le meilleur choix car vous pouvez regarder une zone plus large, ce qui aide à repérer tout ennemi entrant. La vérité est qu'un FoV élevé apporte à la fois des avantages et des inconvénients.
- Un FoV élevé rend le ciblage plus difficile. Plus le FoV est large, plus l'ennemi à l'écran est petit, vous devez donc être plus précis pour les toucher.  
- Un faible FoV vous aide à vous concentrer. Tout sur l'écran est plus grand, il est donc plus facile de se concentrer sur l'endroit où vous visez. 

<img alt="Destiny 2 FOV 85" src="src/d2_fov_85.jpg" title="Destiny 2 FOV 85" width="720" height="405">  
<img alt="Destiny 2 FOV 95" src="src/d2_fov_95.jpg" title="Destiny 2 FOV 95" width="720" height="405">  
<img alt="Destiny 2 FOV 105" src="src/d2_fov_105.jpg" title="Destiny 2 FOV 105" width="720" height="405">  

Il est important de souligner que le FoV se mesure différemment selon les jeux :
- Vertical Degrees (R6: Siege, ...)
- Horizontal 16:9 (Destiny 2, Overwatch, Apex Legends, ...)
- Horizontal 4:3 (CS: GO, ...)
N'oubliez pas cette différence si vous essayez de définir le même champ de vision dans tous vos jeux.

L'article [Converting Sensitivity Between FOVs (Focal Length Scaling)](https://themeta-com.translate.goog/converting-sensitivity-between-fovs-focal-length-scaling/?_x_tr_sl=auto&_x_tr_tl=fr&_x_tr_hl=fr&_x_tr_pto=op) explique comment calculer le FoV de son jeu préféré afin de l'appliquer à n'importer qu'elle jeu. De mon côté, je cherche à avoir le FoV de CS: GO sur Destiny 2. Si on reprend l'exemple de l'article, le FoV de CS: GO est un FoV **Horizontal 4:3 de 90**. Destiny 2 quant à lui utilise un FoV **Horizontal 16:9**. Il faut donc convertir le FoV de CS: GO afin de pouvoir l'appliquer sur Destiny 2. Grace à son outil, on voit qu'il faut un FoV **Horizontal 16:9 de 106** sur Destiny 2.

<img alt="CS: GO FoV Conversions" src="src/fovconversions.png" title="CS: GO FoV Conversions" width="311" height="400">

L'outil [Mouse Sensitivity](https://www.mouse-sensitivity.com/) permet de confirmer sa théorie :

<img alt="CS: GO FoV" src="src/csgo_fov.png" title="CS: GO FoV" width="499" height="200">

Des valeurs de FoV très faibles peuvent également entraîner le mal des transports et des problèmes dans les combats rapprochés, car il est plus difficile de garder une cible en mouvement rapide sur votre écran, tandis qu'avec un FoV élevé, il est plus simple de repérer et de suivre les ennemis. Si les performances sont un problème, sachez que l'augmentation du FoV aura un impact négatif sur vos FPS car votre PC doit travailler plus fort pour afficher davantage à l'écran.  
Sur Destiny 2 (et autres), que vous ayez un FoV de 55 ou de 105, vous parcourez la même distance avec le même temps. Idem pour faire un 360, vous mettez le même temps. Si vous êtes un ancien joueur console, le FoV est de 75 pour les consoles de dernières générations.

### Paramêtre Vidéo

|                       | noobs2pro [4] |  Benny [5] |    Perso   |
|-----------------------|:-------------:|:----------:|:----------:|
| Windows Mode          |   Fullscreen  | Fullscreen | Fullscreen |
| Resolution            |       -       |      -     |      -     |
| Vsync                 |      Off      |     Off    |     Off    |
| Framerate Cap Enabled |      Off      |     Off    |     Off    |
| Framerate Cap         |      200      |     240    |     200    |
| Field of View         |      85       |     105    |     105    |
| Screen Bounds         |       -       |      -     |      -     |
| Brightness            |       -       |      -     |      -     |

**Vsync** : désactivez-le pour chaque jeu, cela ne fait qu'entraver les performances.  
**Framerate Cap Enabled** : la désactivation de la limite de framerate supprimera toutes les limites de FPS.  
**Framerate Cap** : limite le nombre de FPS si vous activez l'option ci-dessus.  
**Field of View** : le champ de vision.
**Screen Bounds** : permet de réduire ou de rapprocher les infos comme le radar, score, ...

<img alt="Destiny 2 Screen Bounds Max" src="src/screen_bounds_max.jpg" title="Destiny 2 Screen Bounds Max" width="720" height="405">  
<img alt="Destiny 2 Screen Bounds Min" src="src/screen_bounds_mini.jpg" title="Destiny 2 Screen Bounds Max" width="720" height="405">  

### Paramêtre Vidéo Avancés

|               | noobs2pro [4] | Benny [5] | Perso |
|---------------|:-------------:|:---------:|:-----:|
| Anti-Aliasing |      Off      |    Off    |  Off  |

**L'anti-aliasing** diminue la visibilité des bords irréguliers, ce qui rend tout plus lisse, plus joli et plus réaliste. Dans Destiny 2 , des options de post-traitement rapide FXAA et SMAA sont proposées, réduisant l'aliasing sur tout l'écran pour un coût minime. [3]  
Je vous conseille de le désactiver pour les performances et les FPS.  

![D2 Anti-Aliasing Perf](src/destiny-2-anti-aliasing-performance-640px.png "Destiny 2 Anti-Aliasing Perfomance")

<ins>1920x1080 Interactive Comparisons :</ins>
- [SMAA vs. FXAA](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-anti-aliasing-interactive-comparison-001-smaa-vs-fxaa-1920x1080.html)  
- [SMAA vs. Off](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-anti-aliasing-interactive-comparison-001-smaa-vs-off-1920x1080.html)  
- [FXAA vs. Off](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-anti-aliasing-interactive-comparison-001-fxaa-vs-off-1920x1080.html)  

----

|                                 | noobs2pro [4] | Benny [5] | Perso |
|---------------------------------|:-------------:|:---------:|:-----:|
| Screenn Space Ambient Occlusion |      Off      |    Off    |  Off  |

**Screen Space Ambient Occlusion (SSAO)** ajoute des ombres de contact là où deux surfaces ou objets se rencontrent et où un objet empêche la lumière d'atteindre un autre élément de jeu à proximité. La technique AO utilisée et la qualité de la mise en œuvre affectent la précision de l'ombrage et la formation de nouvelles ombres lorsque le niveau d'occlusion est faible. [3]  
Je recommande de désactiver cette option pour un max de FPS.

![D2 SSAO Perf](src/destiny-2-screen-space-ambient-occlusion-performance-640px.png "Destiny 2 Screen Space Ambient Occlusion Performance")

<ins>Interactive Comparisons :</ins>
- [3D vs. HDAO](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-screen-space-ambient-occlusion-interactive-comparison-001-3d-vs-hdao.html)  
- [3D vs. Off](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-screen-space-ambient-occlusion-interactive-comparison-001-3d-vs-off.html)  
- [HDAO vs. Off](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-screen-space-ambient-occlusion-interactive-comparison-001-hdao-vs-off.html)  

----

|                    | noobs2pro [4] | Benny [5] | Perso |
|--------------------|:-------------:|:---------:|:-----:|
| Texture Anisotropy |      Off      |    Off    |  2x   |

"**Texture Filtering**", "**Anisotropic Filtering**", "**Texture Anisotropy**", et d'autres options du même nom, affectent la netteté des textures, en particulier celles vues au loin, sur des angles obliques ou sur les côtés de l'écran. Sans filtrage, les surfaces apparaissent floues et la qualité de l'image peut être considérablement dégradée. [3]  
Je recommande l'option **x2** qui permet d'avoir un meilleur rendue sans forcément avoir d'impacte sur le nombre de FPS.  

<img alt="D2 Texture Anisotropy" src="src/destiny-2-texture-anisotropy-performance.png" title="Destiny 2 Texture Anisotropy Performance" width="640" height="421">

<ins>Interactive Comparisons :</ins>
- [2x vs. Off](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-texture-anisotropy-interactive-comparison-001-2x-vs-off.html)  
- [4x vs. Off](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-texture-anisotropy-interactive-comparison-001-4x-vs-off.html)  
- [8x vs. Off](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-texture-anisotropy-interactive-comparison-001-8x-vs-off.html)  
- [16x vs. Off](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-texture-anisotropy-interactive-comparison-001-16x-vs-off.html)  

----

|                 | noobs2pro [4] | Benny [5] | Perso    |
|-----------------|:-------------:|:---------:|:--------:|
| Texture Quality |  Medium/High  |  Highest  |  Highest |

Le paramètre **Texture Quality** de Destiny 2 ajuste la clarté de chaque texture, affectant chaque surface et objet, dans chaque zone des vastes paramètres régionaux de Destiny 2. Plus précisément, la [résolution mipmap](https://en.wikipedia.org/wiki/Mipmap) des textures proches et éloignées est mise à l'échelle pour réduire les besoins en RAM vidéo (VRAM), permettant aux anciens GPU de charger toutes les textures nécessaires dans leur mémoire limitée, évitant ainsi le bégaiement et les pauses dues au chargement et au déchargement fréquents de textures qui ne rentraient pas dans l'espace disponible. [3]  
Ce paramètre dépend de la VRAM de votre carte graphique. Réglez sur **Medium** si vous avez une carte de 2 Go ou **High** si vous avez une carte de 4 Go. 6 Go ou plus peuvent aller pour une qualité de texture **Highest**.

<img alt="D2 Texture Quality" src="src/destiny-2-texture-quality-performance.png" title="Destiny 2 Texture Quality Performance" width="640" height="421">

<ins>Interactive Comparisons :</ins>
- [Low vs. Lowest](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-texture-quality-interactive-comparison-001-low-vs-lowest.html)  
- [Medium vs. Lowest](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-texture-quality-interactive-comparison-001-medium-vs-lowest.html)  
- [High vs. Lowest](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-texture-quality-interactive-comparison-001-high-vs-lowest.html)  
- [Highest vs. Lowest](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-texture-quality-interactive-comparison-001-highest-vs-lowest.html)  

----

|                | noobs2pro [4] | Benny [5] |  Perso |
|----------------|:-------------:|:---------:|:------:|
| Shadow Quality |     Lowest    |   Lowest  | Lowest |

Les ombres sont cruciales pour créer des mondes crédibles et immersifs, et pour améliorer les histoires cinématographiques et les scènes coupées. En tant que tel, il est recommandé de donner la priorité à l'activation d'ombres de meilleure qualité par rapport à l'ajout d'effets supplémentaires et d'autres subtilités dans presque tous les jeux. [3]  
C'est l'un des paramètres graphiques qui a le plus d'impact sur les performances et les FPS dans Destiny 2.  

<img alt="D2 Shadow Quality" src="src/destiny-2-shadow-quality-performance.png" title="Destiny 2 Shadow Quality Performance" width="640" height="421">

<ins>Interactive Comparisons :</ins>
- [Low vs. Lowest](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-shadow-quality-interactive-comparison-001-low-vs-lowest.html)  
- [Medium vs. Lowest](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-shadow-quality-interactive-comparison-001-medium-vs-lowest.html)  
- [High vs. Lowest](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-shadow-quality-interactive-comparison-001-high-vs-lowest.html)  
- [Highest vs. Lowest](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-shadow-quality-interactive-comparison-001-highest-vs-lowest.html)  

----

|                | noobs2pro [4] | Benny [5] | Perso |
|----------------|:-------------:|:---------:|:-----:|
| Depth of Field |      Off      |    Off    |  Off  |

Le flou , le flou stylistique et la profondeur de champ (**Depth of Field**) sont appliqués pendant les cinématiques du jeu et les moments scénarisés, dans certains cas à la troisième personne dans les espaces sociaux et lors de la visée. [3]  
La désactiver aidera à améliorer les performances dans Destiny 2 et vous permetra d'avoir une "meilleur vue" pour viser.

<img alt="D2 Depth of Field" src="src/destiny-2-depth-of-field-performance.png" title="Destiny 2 Depth of Field Performance" width="640" height="421">

<ins>Interactive Comparisons :</ins>
- [Low vs. Off](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-depth-of-field-interactive-comparison-002-low-vs-off.html)  
- [High vs. Off](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-depth-of-field-interactive-comparison-002-high-vs-off.html)  
- [Highest vs. Off ](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-depth-of-field-interactive-comparison-002-highest-vs-off.html)  

---

|                               | noobs2pro [4] | Benny [5] |  Perso |
|-------------------------------|:-------------:|:---------:|:------:|
| Environmental Detail Distance |  Medium/High  |   Low     | Medium |

Comme la distance de détail du personnage, **Environmental Detail Distance** modifie le niveau de détail, bien que dans ce cas, elle affecte les bâtiments, les falaises, les rochers plus gros, les routes et une variété d'autres objets. Et en fonction de leur distance par rapport à la caméra et de leur taille à l'écran, les objets affectés peuvent simplement être affichés à un niveau de détail inférieur, ou entièrement supprimés, transformant une route fissurée en une surface plane, par exemple. [3]  
Vous en aurez besoin pour repérer les ennemis. Gardez-le entre **Medium/High**, cela coûtera que quelques FPS.

<img alt="D2 Environmental Detail Distance" src="src/destiny-2-environmental-detail-distance-performance.png" title="Destiny 2 Environmental Detail Distance Performance" width="640" height="421">

<ins>Interactive Comparisons :</ins>
- [Medium vs. Low](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-environmental-detail-distance-interactive-comparison-001-medium-vs-low.html)  
- [High vs. Low](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-environmental-detail-distance-interactive-comparison-001-high-vs-low.html)  

---

|                           | noobs2pro [4] | Benny [5] | Perso |
|---------------------------|:-------------:|:---------:|:-----:|
| Character Detail Distance |  Medium/High  |    Low    |  High |

Pour éviter de paralyser les performances, les jeux diminuent le niveau de détail (Level of Detail = LoD) des modèles, effets et autres éléments à mesure que leur distance par rapport au point de vue du joueur augmente. Dans Destiny 2, le paramètre Distance des détails du personnage contrôle la LoD des maillages des personnages et des PNJ vus à moyenne et longue portée, avec des distances croissantes élevées à 200 % par rapport à 100 % pour les moyennes et des détails faibles décroissants à 80 %. En pratique, cela se traduit par un ajustement à peine visible de leur qualité d'image. [3]    
Quelque soit le paramêtre, vous n'aurez pas de perte niveau FPS et comme expliqué, l'ajustement est à peine visible.  

<img alt="D2 Character Detail Distance" src="src/destiny-2-character-detail-distance-performance.png" title="Destiny 2 Character Detail Distance Performance" width="640" height="421">

<ins>Interactive Comparisons :</ins>
- [Medium vs. Low](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-character-detail-distance-interactive-comparison-001-medium-vs-low.html)  
- [High vs. Low](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-character-detail-distance-interactive-comparison-001-high-vs-low.html)  

---

|                         | noobs2pro [4] | Benny [5] | Perso |
|-------------------------|:-------------:|:---------:|:-----:|
| Foliage Detail Distance |      Low      |    Low    |  Low  |

Si vous ne l'aviez pas déjà deviné, **Foliage Detail Distance** affecte principalement le niveau de détail et la visibilité des fleurs, des plantes, des buissons, des arbres, de l'herbe et d'autres morceaux de feuillage dans Destiny 2. En High, les distances de détail sont mises à l'échelle à 180 % de la normale. (le paramètre Medium), tandis que Low voit les distances de détail réduites à 50 % de la normale. [3]  
Moins d'arbres et d'herbe sont le meilleur choix en PvP et vous gagnerez en FPS.

<img alt="D2 Foliage Detail Distance" src="src/destiny-2-foliage-detail-distance-performance.png" title="Destiny 2 Foliage Detail Distance Performance" width="640" height="421">

<ins>Interactive Comparisons :</ins>
- [Medium vs. Low](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-foliage-detail-distance-interactive-comparison-001-medium-vs-low.html)  
- [High vs. Low](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-foliage-detail-distance-interactive-comparison-001-high-vs-low.html)  

---

|                          | noobs2pro [4] | Benny [5] |  Perso |
|--------------------------|:-------------:|:---------:|:------:|
| Foliage Shadows Distance |     Medium    |   Medium  | Medium |

Comme son nom l'indique, **Foliage Shadows Distance** ajuste la distance à laquelle les ombres sur les plantes, les arbres, l'herbe et d'autres morceaux de feuillage sont rendues. Dans les coulisses, il y a une augmentation des cascades d'ombres de feuillage allant de Medium à High, et une augmentation des cascades d'ombres de feuillage et d'arbres sur le plus élevé, permettant aux ombres de meilleure qualité d'être rendues plus près des joueurs, améliorant ainsi la qualité globale de l'image. [3]  
Similaire à la **Foliage Detail Distance**, cela ne concerne que les ombres.  

<img alt="D2 Foliage Shadows Distance" src="src/destiny-2-foliage-shadows-distance-performance.png" title="Destiny 2 Foliage Shadows Distance Performance" width="640" height="421">

<ins>Interactive Comparisons :</ins>
- [High vs. Medium](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-foliage-shadows-distance-interactive-comparison-001-high-vs-medium.html)  
- [Highest vs. Medium](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-foliage-shadows-distance-interactive-comparison-001-highest-vs-medium.html)  

---

|              | noobs2pro [4] | Benny [5] |  Perso |
|--------------|:-------------:|:---------:|:------:|
| Light Shafts |     Medium    |   Medium  | Medium |

Certains les appellent **God Rays**, d'autres **Light Rays** ; Bungie les appelle **Light Shafts**. « Eux » étant des rayons crépusculaires, des rayons de lumière brillants que l'on peut voir en regardant le Soleil ou la Lune, en regardant la lumière briller dans une pièce ou une zone sombre, lorsqu'une lumière vive brille à travers des effets environnementaux ou des nuages simulés, ou lorsque la lumière vive croise un objet. [3]  
Pas utilte en PvP.  

<img alt="D2 Light Shafts" src="src/destiny-2-light-shafts-performance.png" title="Destiny 2 Light Shafts Performance" width="640" height="421">

---

|             | noobs2pro [4] | Benny [5] | Perso |
|-------------|:-------------:|:---------:|:-----:|
| Motion Blur |      Off      |    Off    |  Off  |

Si vous voulez une sensation supplémentaire de vitesse lorsque vous vous précipitez à pied ou que vous vous promenez sur un moineau, activez Motion Blur pour ajouter divers degrés de flou en fonction de la vitesse à votre jeu. Les capacités de combat et spéciales ajoutent également différents niveaux de flou, ce qui, pour certains joueurs, peut améliorer l'expérience. Dans les matchs compétitifs de l'Épreuve PvP, cependant, vous souhaiterez peut-être désactiver le flou de mouvement pour une visibilité maximale des ennemis et des attaques. [3]  

<img alt="D2 Motion Blur" src="src/destiny-2-motion-blur-performance.png" title="Destiny 2 Motion Blur Performance" width="640" height="421">

---

|              | noobs2pro [4] | Benny [5] | Perso |
|--------------|:-------------:|:---------:|:-----:|
| Wind Impulse |      Off      |    Off    |  Off  |

Les impacts de balles, les grenades, les super capacités, les bruits de pas et même les propulseurs de véhicules provoquent le déplacement et la courbure du feuillage lorsque le paramètre Wind Impulse est activé. [3]  
Le garder désactivé peut donner un coup de pouce aux FPS, au choix.

<img alt="D2 Wind Impulse" src="src/destiny-2-wind-impulse-performance.png" title="Destiny 2 Wind Impulse Performance" width="640" height="421">

<ins>Interactive Comparisons :</ins>
- [On vs Off](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-wind-impulse-interactive-comparison-001-on-vs-off.html)  

---

|                   | noobs2pro [4] | Benny [5] | Perso |
|-------------------|:-------------:|:---------:|:-----:|
| Render Resolution |      100%     |    100%   |  100% |

Comme NVIDIA DSR, **Render Resolution** rend l'action à une résolution plus élevée et plus détaillée, et réduit automatiquement le résultat à la résolution de l'écran de votre moniteur. Tout sera donc vu à un niveau de fidélité nettement plus élevé, semblera plus net, plus clair et aura beaucoup moins de crénelage. Et en étant directement intégré au jeu, **Render Resolution** a une plus grande flexibilité que DSR, permettant aux joueurs de Destiny 2 de définir n'importe quelle valeur entre 25 % et 200 %, permettant aux joueurs de choisir entre de meilleures performances ou de meilleurs visuels. [3]  
Je vous recommande de ne pas modifier ce paramètre, cela rendra le gameplay flou et vous ne pourrez pas repérer les ennemis facilement.  

<img alt="D2 Render Resolution" src="src/destiny-2-render-resolution-performance.png" title="Destiny 2 Render Resolution Performance" width="640" height="421">

<ins>Interactive Comparisons :</ins>
- [125% vs 100%](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-render-resolution-interactive-comparison-002-125-percent-vs-100-percent-1920x1080.html)  
- [150% vs 100%](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-render-resolution-interactive-comparison-002-150-percent-vs-100-percent-1920x1080.html)  
- [175% vs 100%](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-render-resolution-interactive-comparison-002-175-percent-vs-100-percent-1920x1080.html)  
- [200% vs 100%](https://images.nvidia.com/geforce-com/international/comparisons/destiny-2/destiny-2-render-resolution-interactive-comparison-002-200-percent-vs-100-percent-1920x1080.html)  

---

|                      | noobs2pro [4] | Benny [5] | Perso |
|----------------------|:-------------:|:---------:|:-----:|
| Chromatic Aberration |      Off      |    Off    |  Off  |
| Film Grain           |      Off      |    Off    |  Off  |

**Chromatic Aberration** : Ce ne sont que des effets graphiques inutiles qui provoquent un décalage sur les systèmes low-end.
**Film Grain** : Le grain du film génère un effet filmogène qui gêne la vision. Ce paramètre a une mauvaise influence sur vos performances et votre gameplay, je suggère donc de le désactiver.  

---

### Résumer

|                                 | noobs2pro [4] |  Benny [5] |    Perso   |
|---------------------------------|:-------------:|:----------:|:----------:|
| Windows Mode                    |   Fullscreen  | Fullscreen | Fullscreen |
| Resolution                      |       -       |      -     |      -     |
| Vsync                           |      Off      |     Off    |     Off    |
| Framerate Cap Enabled           |      Off      |     Off    |     Off    |
| Framerate Cap                   |      200      |     240    |     200    |
| Field of View                   |       85      |     105    |     105    |
| Screen Bounds                   |       -       |      -     |      -     |
| Brightness                      |       -       |      -     |      -     |
| Anti-Aliasing                   |      Off      |     Off    |     Off    |
| Screenn Space Ambient Occlusion |      Off      |     Off    |     Off    |
| Texture Anisotropy              |      Off      |     Off    |     2x     |
| Texture Quality                 |  Medium/High  |   Highest  |   Highest  |
| Shadow Quality                  |     Lowest    |   Lowest   |   Lowest   |
| Depth of Field                  |      Off      |     Off    |     Off    |
| Environmental Detail Distance   |  Medium/High  |     Low    |   Medium   |
| Character Detail Distance       |  Medium/High  |     Low    |    High    |
| Foliage Detail Distance         |      Low      |     Low    |     Low    |
| Foliage Shadows Distance        |     Medium    |   Medium   |   Medium   |
| Light Shafts                    |     Medium    |   Medium   |   Medium   |
| Motion Blur                     |      Off      |     Off    |     Off    |
| Wind Impulse                    |      Off      |     Off    |     Off    |
| Render Resolution               |      100%     |    100%    |    100%    |
| Chromatic Aberration            |      Off      |     Off    |     Off    |
| Film Grain                      |      Off      |     Off    |     Off    |

## CVARS Settings

Le fichier `cvars.xml` est le fichier contenant l'ensemble de vos paramêtres de Destiny 2 (paramètre vidéo, touche clavier, ...). Une fois que votre jeux est bien paramétré, n'hésitez pas à le sauvegarder. Vous pouvez aussi vous créer un `cvars.xml` orienté PvP (maxium FPS, peu de latence) et un autre orienté PvE (tous les graphiques à fond !).  

**Emplacement du fichier :** `%appdata%\Bungie\DestinyPC\prefs`

![](src/cvars_settings_1080p60_x264.mp4)
- Copier / coller l'emplacement du fichier.
- Si besoin, faite une sauvegarde de `cvars.xml`, copier / coller le sur votre bureau (ou ailleurs).
- Ouvrir `cvars.xml` via Bloc Note / [Sublime Text](https://www.sublimetext.com/) / [Notepad++](https://notepad-plus-plus.org/).
- Via le raccourcis clavier "CTRL + F", rechercher les paramètre à modifier :
	- `<cvar name="low_latency_mode" value="1" />` -> la valeur doit être égale à `0`.
	- `<cvar name="mouse_smoothing_mode" value="2" />` -> la valeur doit être égale à `0`.
- Résultat :
```xml
<?xml version="1.0"?>
<body>
	<namespace name="graphics">
		...
		<cvar name="low_latency_mode" value="0" />
		...
	</namespace>
	<namespace name="key bindings">
		...
	</namespace>
	<namespace name="audio">
		...
	</namespace>
	<namespace name="gameplay">
		<cvar name="mouse_smoothing_mode" value="0" />
	</namespace>
</body>
```
- Sauvegarder bien le fichier avant de le fermer.

## Steam Launch Options

Steam launch options est très utilisé pour CS:GO. Pour Destiny 2, il existe plusieurs tuto et généralement ils proposent tous la syntaxe suivante : `-high -USEALLAVAILABLECORES`. J'ai rajouté personnellement la commande `-freq 240`.  
`-high` : définis la priorioté du processus (Destiny2.exe) à haut.  
`-USEALLAVAILABLECORES` : active tous les coeurs du processeurs.  
`-freq 240` : permet forcer un taux de rafraîchissement spécifique (tout dépend de votre écran).  

![](src/steam_launch_options_01_1080p60_x264.mp4)
- Lancez steam.
- Sélectionnez Destiny 2.
- Sur la droite, cliquer sur "paramètre" et "proprétés...".
- Dans l'onglet "général" copier / coller `-freq 240 -high -USEALLAVAILABLECORES` dans "options de lancement".
- Fermer la fenêtre.

Malheuresement, le paramêtre `-high` ne fonctionne pas. Une fois le jeu lancé, la processus Destiny 2 a une priorité "normal" (voir la vidéo ci dessus). J'ai donc un doute sur la fiabilité de ces commandes. Puis je n'ai pas trouvé de documentation officiel.  

Pour forcer le processus `destiny2.exe` & `destiny2launcher.exe` à une priorité haute, il faudra le faire manuellement à chaque fois que vous lancez le jeu.

![](src/steam_launch_options_02_1080p60_x264.mp4)
- Lancez Destiny 2.
- Une fois le jeu lancé, fair un "ALT+TAB" pour basculer sur votre bureau.
- Faire un clique droit la barre des tâches puis "Gestionnaire des taches".
- Dans l'onglet "processus", cliquer sur "Destiny 2", clique droit puis "Accéder aux détails".
- Sélectionner `destiny2.exe`, clique droit "définir la priorité : haute".
- Sélectionner `destiny2launcher.exe`, clique droit "définir la priorité : haute".
- Faire "ALT+TAB" pour retourner sur Destiny 2.

Il est aussi possible de faire un script.

![](src/steam_launch_options_03_1080p60_x264.mp4)
- Ouvrir l'application "Bloc-notes" de Windows.
- Copier / coller les lignes suivantes :
```bash
wmic process where name='destiny2.exe' call setpriority 128
wmic process where name='destiny2launcher.exe' call setpriority 128
```
- Sauvegarder le document via "Fichier / Enregistrer sous...".
- Sélectionner votre bureau, puis nommé le `destiny2-process-hight.bat`.
- Lancez Destiny 2.
- Une fois le jeu lancé, fair un "ALT+TAB" pour basculer sur votre bureau.
- Executer `destiny2-process-hight.bat` en double cliquant dessus.
- Faire "ALT+TAB" pour retourner sur Destiny 2.

<ins>Source :</ins>

- ESPORTS TALES (??.??.20??) : [How to increase FPS in Destiny 2: cvars, video settings, launch options](https://www.esportstales.com/destiny/how-to-increase-fps-cvars-video-settings-launch-options) [1]
- ESPORTS TALES(??.??.20??) : [The best FoV (Field of View) for Destiny 2](https://www.esportstales.com/destiny/the-best-fov-field-of-view) [2]
- NVIDIA (17.10.2017) : [Destiny 2 PC Graphics and Performance Guide](https://www.nvidia.com/en-us/geforce/news/destiny-2-pc-graphics-and-performance-guide/) [3]
- Noobs2pro (21.01.2020) : [Best Destiny 2 graphics settings for Gaming and FPS](https://www.noobs2pro.com/best-performance-and-graphics-settings-destiny-2/) [4]
- YouTube Video by Benny (30.05.2021) : [My in-depth Destiny 2 Settings](https://youtu.be/zfoiWN1fS2s) [5]
- YouTube Video by Gjerda (22.07.2021) : [10 PC settings to lower latency in Destiny 2](https://youtu.be/Nj5W2qb6no0) [6]